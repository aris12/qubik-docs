###################
Command and Control
###################

Intro
=====
QUBIK pocketqube twins are radio amateur service satellites that can be commanded by radio amateurs around the world. The purpose of documenting this functionality is to enable more radio amateurs around the world to gain hands-on experience with C&C of an actual satellite, testing their setups and having a rewarding experience.

.. warning::
    Licensed radio amateurs only!

    Before attempting to transmit any signals to QUBIK satellites please ensure that you are compliant with any local laws and regulations!
    If uncertain, reach out to your national radio amateur association.

Hardware setup
==============
A typical command and control station for QUBIK satellites would include the following hardware components:

#. UHF yagi Antenna(s) (e.g. XYA043532)
#. Power amplifier (e.g. Mitsubishi RA07H4047M 7W PA - `more info <https://gitlab.com/librespacefoundation/qubik/qubik-org/-/wikis/Power-Ampilifier-For-Ground-Station>`_)
#. LNA (e.g. SSB LNA-5000)
#. RF Switch
#. Appropriate cabling
#. Rotator with controller (e.g. Yaesu G-5500)
#. SDR TX capable (e.g. PlutoSDR)
#. One or two host computers depending on the setup (with Linux OS).

.. list-table:: 

    * - .. figure:: img/1-ant.png

           one antenna setup

      - .. figure:: img/2-ant.png

           two antennas setup



.. figure:: img/qubik_tx_1.jpg
   :width: 800px
   :alt: QUBIK TX setup
   :align: center

   Indicative QUBIK TX setup.


Software setup
==============
In order to command and control a QUBIK satellite you will need a specific software stack to handle all the tasks necessary.

1. `GNURadio 3.8.2 <https://www.gnuradio.org/>`_
2. `gr-soapy (and appropiate sdr specific drivers) <https://gitlab.com/librespacefoundation/gr-soapy>`_
3. `gr-satnogs <https://gitlab.com/librespacefoundation/satnogs/gr-satnogs>`_
4. `gpredict (for doppler and rotator control) <http://gpredict.oz9aec.net/>`_
5. `QUBIK transceiver flowgraph <https://gitlab.com/librespacefoundation/qubik/qubik-comms-sw/-/blob/master/test/flowgraphs/qubik_transceiver.grc>`_ (The GNURadio transceiver)
6. `osdlp-operator <https://gitlab.com/librespacefoundation/osdlp-operator>`_ (The command and control program)
7. `qubik_listener <https://gitlab.com/librespacefoundation/qubik/qubik_listener>`_ (The packet forwarder to SatNOGS DB)


.. figure:: img/qubik_tx_sw.png
   :width: 800px
   :alt: QUBIK TX Software
   :align: center

   QUBIK Command Software diagram

.. tip::
   Your command software stack can be split in two different setups.
   
   e.g. A Rpi4 computer near the antenna with the SDR attached to it controlling also the rotator, while a second host over the network can handle the command and control software (osdlp-operator) and the gpredict (for doppler and rotator pointing calculations) for a pass. Of course those can be hosted under a single computer if cabling and secifics allow.

.. tip::
   Setup your Gpredict Radio Interface with Radio Type: Full-Duplex and leave anything alse as default.

Running
=======

When all hardware and software are ready and installed, just before AOS the following should happen:

#. Run :code:`rigctld -T 127.0.0.1 -m 1 -v` to create a rigctl server to wait for incoming clients (transceiver and gpredict)
#. Run the appropiate :code:`rotctld` command to wait for incoming gpredict connection
#. Run the :code:`qubik_transceiver.py` with the appropiate arguments
   
   * For PlutoSDR :code:`./qubik_transceiver.py --soapy-rx-device 'driver=plutosdr' --antenna-rx 'A_BALANCED' --antenna-tx 'A_BALANCED'`

   * For USRP use: :code:`./qubik_transceiver.py --soapy-rx-device 'driver=uhd' --antenna-rx 'RX2' --antenna-tx 'TX/RX'`

#. Run the osdlp-operator with the fop configuration: :code:`osdlp-operator fop_configuration.cfg`
#. Run :code:`netcat -lu -p 16880` to see all debugging messages from osdlp-operator
#. Run :code:`gpredict` and start the pass operations (Antenna and Radio control) for your selected QUBIK
#. Run :code:`qubik_listener.py` to forward any received frames to SatNOGS DB (and see them locally)

   * Example for station :code:`./qubik_listener.py --callsign SV1QVE --lon 36.970945N --lat 36.970945E`

#. Return to osdlp-operator and command QUBIKs!

.. note::
   If your running session is a testing one (with QUBIK EMs) you *need* to disable the rigctl message block from the qubik_transceiver flowgraph, for your session to run. No need to doppler compensate on ground!
